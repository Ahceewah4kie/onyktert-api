FROM openjdk:8-jre

RUN mkdir -p /opt/app
ENV PROJECT_HOME /opt/app

ENTRYPOINT ["java", "-jar", "/opt/app/onyktert.jar"]
# Add the service itself
ARG JAR_FILE
ADD target/${JAR_FILE} /opt/app/onyktert.jar

WORKDIR $PROJECT_HOME

CMD ["java", "-Dspring.data.mongodb.uri=mongodb://onyktert-mongodb:27017/onyktert","-Djava.security.egd=file:/dev/./urandom","-jar","/opt/app/Onyktert-1.0-SNAPSHOT.jar"]