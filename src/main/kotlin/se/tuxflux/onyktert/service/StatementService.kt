package se.tuxflux.onyktert.service

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import se.tuxflux.onyktert.exception.DoesNotExistsException
import se.tuxflux.onyktert.model.Statement
import se.tuxflux.onyktert.model.StatementType
import se.tuxflux.onyktert.model.Statements
import se.tuxflux.onyktert.model.SubmittedStatement
import se.tuxflux.onyktert.repository.StatementRepository
import se.tuxflux.onyktert.service.helper.FilterService
import se.tuxflux.onyktert.service.helper.SanitationService
import se.tuxflux.onyktert.service.helper.ValidationService
import java.util.*

interface StatementService {
    fun getNeverHaveIEver(includeSfw: Boolean, includeNsfw: Boolean): Statements
    fun getPointingGame(includeSfw: Boolean, includeNsfw: Boolean): Statements
    fun getInOtherWords(includeSfw: Boolean, includeNsfw: Boolean): Statements
    fun getTruthOrDareTruths(includeSfw: Boolean, includeNsfw: Boolean): Statements
    fun getTruthOrDareDares(includeSfw: Boolean, includeNsfw: Boolean): Statements

    fun addStatement(statement: Statement)
    fun deleteStatement(id: Long)
}

@Service
class StatementServiceImpl : StatementService {
    private final val logger: Logger = LoggerFactory.getLogger("StatementService")

    @Autowired
    lateinit var repository: StatementRepository

    @Autowired
    lateinit var filterService: FilterService
    @Autowired
    lateinit var sanitationService: SanitationService
    @Autowired
    lateinit var validationService: ValidationService

    override fun addStatement(statement: Statement) {
        val conflictingStatements = repository.findByText(statement.text)
        removeConflictingStatements(conflictingStatements) // Remove (ie. replace) statements with same text

        statement.id = validateId(statement.id)
        validationService.validateStatement(statement)
        val sanitizedStatement = sanitationService.sanitizeStatement(statement)

        logger.info("Persisting statement: $sanitizedStatement")
        repository.save(sanitizedStatement)
    }

    override fun deleteStatement(id: Long) {
        val statement = repository.findById(id)
        if (!statement.isPresent) {
            throw DoesNotExistsException("'$id' does not exist")
        }
        logger.info("Removing statement: $statement")
        repository.deleteById(id)
    }

    override fun getNeverHaveIEver(includeSfw: Boolean, includeNsfw: Boolean): Statements {
        logger.info("Fetching ${fetchType(includeSfw, includeNsfw)} StatementType.NEVER_HAVE_I_EVER")
        val result: List<Statement> = repository
                .findByType(StatementType.NEVER_HAVE_I_EVER)
                .filter { statement ->
                    filterService.nsfwFilter(statement.isNsfw, includeSfw, includeNsfw)
                }
        return Statements(statements = result)
    }

    override fun getPointingGame(includeSfw: Boolean, includeNsfw: Boolean): Statements {
        logger.info("Fetching ${fetchType(includeSfw, includeNsfw)} StatementType.POINTING_GAME")
        val result: List<Statement> = repository
                .findByType(StatementType.POINTING_GAME)
                .filter { statement ->
                    filterService.nsfwFilter(statement.isNsfw, includeSfw, includeNsfw)
                }
        return Statements(statements = result)
    }

    override fun getInOtherWords(includeSfw: Boolean, includeNsfw: Boolean): Statements {
        logger.info("Fetching ${fetchType(includeSfw, includeNsfw)} StatementType.IN_OTHER_WORDS")
        val result: List<Statement> = repository
                .findByType(StatementType.IN_OTHER_WORDS)
                .filter { statement ->
                    filterService.nsfwFilter(statement.isNsfw, includeSfw, includeNsfw)
                }
        return Statements(statements = result)
    }

    override fun getTruthOrDareTruths(includeSfw: Boolean, includeNsfw: Boolean): Statements {
        logger.info("Fetching ${fetchType(includeSfw, includeNsfw)} StatementType.TRUTH_OR_DARE_TRUTH")
        val result: List<Statement> = repository
                .findByType(StatementType.TRUTH_OR_DARE_TRUTH)
                .filter { statement ->
                    filterService.nsfwFilter(statement.isNsfw, includeSfw, includeNsfw)
                }
        return Statements(statements = result)
    }

    override fun getTruthOrDareDares(includeSfw: Boolean, includeNsfw: Boolean): Statements {
        logger.info("Fetching ${fetchType(includeSfw, includeNsfw)} StatementType.TRUTH_OR_DARE_DARE")
        val result: List<Statement> = repository
                .findByType(StatementType.TRUTH_OR_DARE_DARE)
                .filter { statement ->
                    filterService.nsfwFilter(statement.isNsfw, includeSfw, includeNsfw)
                }
        return Statements(statements = result)
    }

    private fun removeConflictingStatements(conflictingStatements: List<SubmittedStatement>) {
        for (conflictingStatement in conflictingStatements) {
            logger.info("Removing conflicting statement: $conflictingStatement")
            deleteStatement(conflictingStatement.id)
        }
    }

    private fun fetchType(includeSfw: Boolean, includeNsfw: Boolean): String {
        return if (includeSfw && includeNsfw) {
            "all"
        } else if (includeNsfw) {
            "only NSFW"
        } else {
            "only SFW"
        }
    }

    private fun validateId(id: Long): Long {
        val statement = repository.findById(id)
        return if (statement.isPresent) {
            val newId = id * Random().nextInt(100)
            validateId(newId)
        } else {
            id
        }
    }
}

