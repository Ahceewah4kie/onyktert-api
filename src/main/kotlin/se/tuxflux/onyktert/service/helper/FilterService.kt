package se.tuxflux.onyktert.service.helper

import org.springframework.stereotype.Service

@Service
class FilterService {

    fun nsfwFilter(isNsfw: Boolean, includeSfw: Boolean, includeNsfw: Boolean): Boolean {
        if (isNsfw == true) {
            return includeNsfw == true
        } else {
            return includeSfw == true
        }
    }
}
