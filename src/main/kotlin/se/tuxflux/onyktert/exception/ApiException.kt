package se.tuxflux.onyktert.exception

class AlreadyExistsException(override var message: String) : Exception(message)
class DoesNotExistsException(override var message: String) : Exception(message)
class ForbiddenException(override var message: String) : Exception(message)
class MissingFieldException(override var message: String) : Exception(message)
