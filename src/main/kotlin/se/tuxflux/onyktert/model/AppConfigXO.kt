package se.tuxflux.onyktert.model

class AdConfigXO(vo: AdConfig) {
    fun toVO(): AdConfig = AdConfig(this.id, this.interstitialAdFrequency, this.showInterstitialAd, this.showBannerAd)

    val id = vo.id
    val interstitialAdFrequency = vo.interstitialAdFrequency
    val showInterstitialAd = vo.showInterstitialAd
    val showBannerAd = vo.showBannerAd
}