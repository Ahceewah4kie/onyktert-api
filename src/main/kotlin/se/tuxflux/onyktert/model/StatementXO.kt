package se.tuxflux.onyktert.model

import java.util.*

/**
 * API facing statements for a number of different games
 */
class StatementXO(statement: Statement) {
    val id: Long = statement.id
    val isNsfw: Boolean = statement.isNsfw
    val text: String = statement.text
    val type: StatementType = statement.type
    var createdDate: Date = statement.createdDate

    fun toVO(): Statement = Statement(-1, this.isNsfw, this.text, this.type, this.createdDate)
}


/**
 * API facing user submitted statements
 */
class SubmittedStatementXO(statement: SubmittedStatement) {
    val id: Long = statement.id
    val text: String = statement.text
    val type: StatementType = statement.type
    var createdDate: Date = statement.createdDate
}

/**
 * API facing wrapper for list of statements
 */
data class StatementsXO(var statements: List<StatementXO>)

/**
 * API facing wrapper for list of submitted statements
 */
data class SubmittedStatementsXO(var statements: List<SubmittedStatementXO>)
