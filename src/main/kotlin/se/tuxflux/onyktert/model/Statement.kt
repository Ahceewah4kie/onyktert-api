package se.tuxflux.onyktert.model

import org.springframework.data.annotation.Id
import org.springframework.data.annotation.TypeAlias
import java.util.*

enum class StatementType {
    NEVER_HAVE_I_EVER,
    POINTING_GAME,
    IN_OTHER_WORDS,
    TRUTH_OR_DARE_DARE,
    TRUTH_OR_DARE_TRUTH,
    TRUTH_OR_DARE
}

/**
 * Statements for a number of different games
 */
@TypeAlias("statement")
data class Statement(@Id var id: Long, val isNsfw: Boolean, val text: String, val type: StatementType, var createdDate: Date = Date()) {
    fun toXO(): StatementXO = StatementXO(this)
}


/**
 * User submitted statements
 */
@TypeAlias("submitted_statement")
data class SubmittedStatement(@Id var id: Long, val text: String, val type: StatementType, var createdDate: Date = Date()) {
    fun toXO(): SubmittedStatementXO = SubmittedStatementXO(this)
}


/**
 * Wrapper for a list of statements
 */
data class Statements(val statements: List<Statement>) {
    fun toXO(): StatementsXO = StatementsXO(this.statements.map { statement -> statement.toXO() })
}


/**
 * Wrapper for a list of submitted statements
 */
data class SubmittedStatements(val statements: List<SubmittedStatement>) {
    fun toXO(): SubmittedStatementsXO = SubmittedStatementsXO(this.statements.map { statement -> statement.toXO() })
}
