package se.tuxflux.onyktert.repository

import org.springframework.data.repository.CrudRepository
import se.tuxflux.onyktert.model.AdConfig

interface AdConfigRepository : CrudRepository<AdConfig, Long>