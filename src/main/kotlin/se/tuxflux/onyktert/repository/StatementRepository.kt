package se.tuxflux.onyktert.repository

import org.springframework.data.repository.CrudRepository
import se.tuxflux.onyktert.model.Statement
import se.tuxflux.onyktert.model.StatementType
import se.tuxflux.onyktert.model.SubmittedStatement


interface StatementRepository : CrudRepository<Statement, Long> {
    fun findByType(type: StatementType): List<Statement>
    fun findByText(text: String): List<SubmittedStatement>
}