package se.tuxflux.onyktert.controller

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import se.tuxflux.onyktert.model.AdConfigXO
import se.tuxflux.onyktert.service.ConfigService

@RestController
class AdsController {
    private val logger: Logger = LoggerFactory.getLogger("AdsController")

    @Autowired
    private lateinit var configService: ConfigService

    @GetMapping("/api/v1/config/ads", produces = ["application/json"])
    fun getAdConfig(): AdConfigXO {
        val adConfig = configService.getAdConfig()
        return AdConfigXO(adConfig)
    }

    @PutMapping("/api/v1/config/ads", produces = ["application/json"])
    fun getAdConfig(@RequestBody adConfigXO: AdConfigXO): AdConfigXO {
        val adConfig = configService.setAdConfig(adConfigXO.toVO())
        return AdConfigXO(adConfig)
    }
}