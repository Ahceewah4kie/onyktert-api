package se.tuxflux.onyktert.controller

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import se.tuxflux.onyktert.model.SubmittedStatement
import se.tuxflux.onyktert.model.SubmittedStatementXO
import se.tuxflux.onyktert.model.SubmittedStatementsXO
import se.tuxflux.onyktert.service.SubmittedStatementService
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@RestController
class SubmittedStatementController {
    private val logger: Logger = LoggerFactory.getLogger("GameController")

    @Autowired
    private lateinit var statementService: SubmittedStatementService


    @PostMapping("/api/v1/game/statement", consumes = ["application/json"])
    fun legacyAddSubmittedStatement(@RequestBody statement: SubmittedStatementXO,
                              request: HttpServletRequest, response: HttpServletResponse) {
        statementService.addStatement(SubmittedStatement(id = -1, text = statement.text, type = statement.type, createdDate = Date()))
    }

    @PostMapping("/api/v1/submitted/statement", consumes = ["application/json"])
    fun addSubmittedStatement(@RequestBody statement: SubmittedStatementXO,
                              request: HttpServletRequest, response: HttpServletResponse) {
        statementService.addStatement(SubmittedStatement(id = -1, text = statement.text, type = statement.type, createdDate = Date()))
    }

    @GetMapping("/api/v1/submitted/neverhaveiever", produces = ["application/json"])
    fun neverHaveIEver(@RequestParam(value = "nsfw", required = false, defaultValue = "true") includeNsfw: Boolean,
                       @RequestParam("sfw", required = false, defaultValue = "true") includeSfw: Boolean,
                       request: HttpServletRequest, response: HttpServletResponse): SubmittedStatementsXO {
        val vo = statementService.getNeverHaveIEver(includeSfw, includeNsfw)
        return vo.toXO()
    }

    @GetMapping("/api/v1/submitted/pointinggame", produces = ["application/json"])
    fun pointingGame(@RequestParam(value = "nsfw", required = false, defaultValue = "true") includeNsfw: Boolean,
                     @RequestParam("sfw", required = false, defaultValue = "true") includeSfw: Boolean,
                     request: HttpServletRequest, response: HttpServletResponse): SubmittedStatementsXO {
        val vo = statementService.getPointingGame(includeSfw, includeNsfw)
        return vo.toXO()
    }

    @GetMapping("/api/v1/submitted/inotherwords", produces = ["application/json"])
    fun inOtherWords(@RequestParam(value = "nsfw", required = false, defaultValue = "true") includeNsfw: Boolean,
                     @RequestParam("sfw", required = false, defaultValue = "true") includeSfw: Boolean,
                     request: HttpServletRequest, response: HttpServletResponse): SubmittedStatementsXO {
        val vo = statementService.getInOtherWords(includeSfw, includeNsfw)
        return vo.toXO()
    }

    @GetMapping("/api/v1/submitted/truthordare/truth", produces = ["application/json"])
    fun truth(@RequestParam(value = "nsfw", required = false, defaultValue = "true") includeNsfw: Boolean,
              @RequestParam("sfw", required = false, defaultValue = "true") includeSfw: Boolean,
              request: HttpServletRequest, response: HttpServletResponse): SubmittedStatementsXO {
        val vo = statementService.getTruthOrDareTruths(includeSfw, includeNsfw)
        return vo.toXO()
    }

    @GetMapping("/api/v1/submitted/truthordare/dare", produces = ["application/json"])
    fun dare(@RequestParam(value = "nsfw", required = false, defaultValue = "true") includeNsfw: Boolean,
             @RequestParam("sfw", required = false, defaultValue = "true") includeSfw: Boolean,
             request: HttpServletRequest, response: HttpServletResponse): SubmittedStatementsXO {
        val vo = statementService.getTruthOrDareDares(includeSfw, includeNsfw)
        return vo.toXO()
    }

    @GetMapping("/api/v1/submitted/truthordare", produces = ["application/json"])
    fun truthOrDareMixed(@RequestParam(value = "nsfw", required = false, defaultValue = "true") includeNsfw: Boolean,
             @RequestParam("sfw", required = false, defaultValue = "true") includeSfw: Boolean,
             request: HttpServletRequest, response: HttpServletResponse): SubmittedStatementsXO {
        val vo = statementService.getTruthOrDareMixed(includeSfw, includeNsfw)
        return vo.toXO()
    }
}