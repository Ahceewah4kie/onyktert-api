package se.tuxflux.onyktert.controller

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import se.tuxflux.onyktert.model.StatementXO
import se.tuxflux.onyktert.service.AuthService
import se.tuxflux.onyktert.service.StatementService
import se.tuxflux.onyktert.service.SubmittedStatementService
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@RestController
class AdminController {
    private val logger: Logger = LoggerFactory.getLogger("AdminController")

    @Autowired
    private lateinit var authService: AuthService
    @Autowired
    private lateinit var statementService: StatementService
    @Autowired
    private lateinit var submittedStatementService: SubmittedStatementService

    @PostMapping("/api/v1/admin/statement", consumes = ["application/json"])
    fun addStatement(@RequestHeader(value = "apikey", required = true) apiKey: String,
                     @RequestBody statement: StatementXO,
                     request: HttpServletRequest, response: HttpServletResponse) {
        authService.isValidApiKey(apiKey, throwOnError = true)
        statementService.addStatement(statement.toVO())
    }

    @DeleteMapping("/api/v1/admin/statement/{id}")
    fun deleteStatement(@RequestHeader(value = "apikey", required = true) apiKey: String,
                        @PathVariable("id") id: String,
                        request: HttpServletRequest, response: HttpServletResponse) {
        authService.isValidApiKey(apiKey, throwOnError = true)
        statementService.deleteStatement(id.toLong())
    }

    @DeleteMapping("/api/v1/admin/submitted/statement/{id}")
    fun deleteSubmittedStatement(@RequestHeader(value = "apikey", required = true) apiKey: String,
                                 @PathVariable("id") id: String,
                                 request: HttpServletRequest, response: HttpServletResponse) {
        authService.isValidApiKey(apiKey, throwOnError = true)
        submittedStatementService.deleteStatement(id.toLong())
    }
}
