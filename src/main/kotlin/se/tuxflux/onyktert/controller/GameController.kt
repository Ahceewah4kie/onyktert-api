package se.tuxflux.onyktert.controller

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import se.tuxflux.onyktert.model.StatementsXO
import se.tuxflux.onyktert.service.StatementService
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@RestController
class GameController {
    private val logger: Logger = LoggerFactory.getLogger("GameController")

    @Autowired
    private lateinit var statementService: StatementService

    @GetMapping("/api/v1/game/neverhaveiever", produces = ["application/json"])
    fun neverHaveIEver(@RequestParam(value = "nsfw", required = false, defaultValue = "true") includeNsfw: Boolean,
                       @RequestParam("sfw", required = false, defaultValue = "true") includeSfw: Boolean,
                       request: HttpServletRequest, response: HttpServletResponse): StatementsXO {
        val vo = statementService.getNeverHaveIEver(includeSfw, includeNsfw)
        return vo.toXO()
    }

    @GetMapping("/api/v1/game/pointinggame", produces = ["application/json"])
    fun pointingGame(@RequestParam(value = "nsfw", required = false, defaultValue = "true") includeNsfw: Boolean,
                     @RequestParam("sfw", required = false, defaultValue = "true") includeSfw: Boolean,
                     request: HttpServletRequest, response: HttpServletResponse): StatementsXO {
        val vo = statementService.getPointingGame(includeSfw, includeNsfw)
        return vo.toXO()
    }

    @GetMapping("/api/v1/game/inotherwords", produces = ["application/json"])
    fun inOtherWords(@RequestParam(value = "nsfw", required = false, defaultValue = "true") includeNsfw: Boolean,
                     @RequestParam("sfw", required = false, defaultValue = "true") includeSfw: Boolean,
                     request: HttpServletRequest, response: HttpServletResponse): StatementsXO {
        val vo = statementService.getInOtherWords(includeSfw, includeNsfw)
        return vo.toXO()
    }

    @GetMapping("/api/v1/game/truthordare/truth", produces = ["application/json"])
    fun truth(@RequestParam(value = "nsfw", required = false, defaultValue = "true") includeNsfw: Boolean,
              @RequestParam("sfw", required = false, defaultValue = "true") includeSfw: Boolean,
              request: HttpServletRequest, response: HttpServletResponse): StatementsXO {
        val vo = statementService.getTruthOrDareTruths(includeSfw, includeNsfw)
        return vo.toXO()
    }

    @GetMapping("/api/v1/game/truthordare/dare", produces = ["application/json"])
    fun dare(@RequestParam(value = "nsfw", required = false, defaultValue = "true") includeNsfw: Boolean,
             @RequestParam("sfw", required = false, defaultValue = "true") includeSfw: Boolean,
             request: HttpServletRequest, response: HttpServletResponse): StatementsXO {
        val vo = statementService.getTruthOrDareDares(includeSfw, includeNsfw)
        return vo.toXO()
    }
}