package se.tuxflux.onyktert

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import se.tuxflux.onyktert.model.Statement
import se.tuxflux.onyktert.model.StatementType
import se.tuxflux.onyktert.service.helper.FilterService
import java.util.*

class NsfwTest {
    private val filterService: FilterService = FilterService()
    private val date = Date(1542925478714)
    @Test
    fun onlyNsfw() {
        val statements = arrayListOf(
                Statement(id = 1, isNsfw = false, text = "", type = StatementType.NEVER_HAVE_I_EVER, createdDate = date),
                Statement(id = 2, isNsfw = true, text = "", type = StatementType.NEVER_HAVE_I_EVER, createdDate = date),
                Statement(id = 3, isNsfw = false, text = "", type = StatementType.NEVER_HAVE_I_EVER, createdDate = date),
                Statement(id = 4, isNsfw = true, text = "", type = StatementType.NEVER_HAVE_I_EVER, createdDate = date)
        )
        Assertions.assertEquals(statements.size, 4)

        val filteredStatements = statements.filter { statement -> filterService.nsfwFilter(statement.isNsfw, false, true) }
        Assertions.assertEquals(filteredStatements.size, 2)
        Assertions.assertEquals(Statement(id = 2, isNsfw = true, text = "", type = StatementType.NEVER_HAVE_I_EVER, createdDate = date), filteredStatements[0])
        Assertions.assertEquals(Statement(id = 4, isNsfw = true, text = "", type = StatementType.NEVER_HAVE_I_EVER, createdDate = date), filteredStatements[1])
    }

    @Test
    fun onlySfw() {

        val statements = arrayListOf(
                Statement(id = 1, isNsfw = false, text = "", type = StatementType.NEVER_HAVE_I_EVER, createdDate = date),
                Statement(id = 2, isNsfw = true, text = "", type = StatementType.NEVER_HAVE_I_EVER, createdDate = date),
                Statement(id = 3, isNsfw = false, text = "", type = StatementType.NEVER_HAVE_I_EVER, createdDate = date),
                Statement(id = 4, isNsfw = true, text = "", type = StatementType.NEVER_HAVE_I_EVER, createdDate = date)
        )
        Assertions.assertEquals(statements.size, 4)

        val filteredStatements = statements.filter { statement -> filterService.nsfwFilter(statement.isNsfw, true, false) }
        Assertions.assertEquals(filteredStatements.size, 2)
        Assertions.assertEquals(Statement(id = 1, isNsfw = false, text = "", type = StatementType.NEVER_HAVE_I_EVER, createdDate = date), filteredStatements[0])
        Assertions.assertEquals(Statement(id = 3, isNsfw = false, text = "", type = StatementType.NEVER_HAVE_I_EVER, createdDate = date), filteredStatements[1])
    }

    @Test
    fun both() {

        val statements = arrayListOf(
                Statement(id = 1, isNsfw = false, text = "", type = StatementType.NEVER_HAVE_I_EVER, createdDate = date),
                Statement(id = 2, isNsfw = true, text = "", type = StatementType.NEVER_HAVE_I_EVER, createdDate = date),
                Statement(id = 3, isNsfw = false, text = "", type = StatementType.NEVER_HAVE_I_EVER, createdDate = date),
                Statement(id = 4, isNsfw = true, text = "", type = StatementType.NEVER_HAVE_I_EVER, createdDate = date)
        )
        Assertions.assertEquals(statements.size, 4)

        val filteredStatements = statements.filter { statement -> filterService.nsfwFilter(statement.isNsfw, true, true) }

        Assertions.assertEquals(filteredStatements.size, 4)
        Assertions.assertEquals(Statement(id = 1, isNsfw = false, text = "", type = StatementType.NEVER_HAVE_I_EVER, createdDate = date), filteredStatements[0])
        Assertions.assertEquals(Statement(id = 2, isNsfw = true, text = "", type = StatementType.NEVER_HAVE_I_EVER, createdDate = date), filteredStatements[1])
        Assertions.assertEquals(Statement(id = 3, isNsfw = false, text = "", type = StatementType.NEVER_HAVE_I_EVER, createdDate = date), filteredStatements[2])
        Assertions.assertEquals(Statement(id = 4, isNsfw = true, text = "", type = StatementType.NEVER_HAVE_I_EVER, createdDate = date), filteredStatements[3])
    }

    @Test
    fun none() {
        val statements = arrayListOf(
                Statement(id = 1, isNsfw = false, text = "", type = StatementType.NEVER_HAVE_I_EVER, createdDate = date),
                Statement(id = 2, isNsfw = true, text = "", type = StatementType.NEVER_HAVE_I_EVER, createdDate = date),
                Statement(id = 3, isNsfw = false, text = "", type = StatementType.NEVER_HAVE_I_EVER, createdDate = date),
                Statement(id = 4, isNsfw = true, text = "", type = StatementType.NEVER_HAVE_I_EVER, createdDate = date)
        )
        Assertions.assertEquals(statements.size, 4)

        val filteredStatements = statements.filter { statement -> filterService.nsfwFilter(statement.isNsfw, false, false) }

        Assertions.assertEquals(filteredStatements.size, 0)
    }
}